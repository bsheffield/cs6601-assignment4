import numpy as np
from collections import Counter
import time


class DecisionNode:
    """Class to represent a single node in a decision tree."""

    def __init__(self, left, right, decision_function, class_label=None):
        """Create a decision function to select between left and right nodes.
        Note: In this representation 'True' values for a decision take us to
        the left. This is arbitrary but is important for this assignment.
        Args:
            left (DecisionNode): left child node.
            right (DecisionNode): right child node.
            decision_function (func): function to decide left or right node.
            class_label (int): label for leaf node. Default is None.
        """

        self.left = left
        self.right = right
        self.decision_function = decision_function
        self.class_label = class_label

    def decide(self, feature):
        """Get a child node based on the decision function.
        Args:
            feature (list(int)): vector for feature.
        Return:
            Class label if a leaf node, otherwise a child node.
        """

        if self.class_label is not None:
            return self.class_label

        elif self.decision_function(feature):
            return self.left.decide(feature)

        else:
            return self.right.decide(feature)


def load_csv(data_file_path, class_index=-1):
    """Load csv data in a numpy array.
    Args:
        data_file_path (str): path to data file.
        class_index (int): slice output by index.
    Returns:
        features, classes as numpy arrays if class_index is specified,
            otherwise all as nump array.
    """

    handle = open(data_file_path, 'r')
    contents = handle.read()
    handle.close()
    rows = contents.split('\n')
    out = np.array([[float(i) for i in r.split(',')] for r in rows if r])

    if (class_index == -1):
        classes = out[:, class_index]
        features = out[:, :class_index]
        return features, classes
    elif (class_index == 0):
        classes = out[:, class_index]
        features = out[:, 1:]
        return features, classes

    else:
        return out


def build_decision_tree():
    """Create a decision tree capable of handling the sample data.
    Tree is built fully starting from the root.
    Returns:
        The root node of the decision tree.
    """

    dt_root = DecisionNode(None, None, lambda feature: feature[0] == 0)
    dt_n2 = DecisionNode(None, None, lambda feature: feature[1] == 0)
    dt_n3 = DecisionNode(None, None, lambda feature: feature[2] == 0)
    dt_n4 = DecisionNode(None, None, lambda feature: feature[3] == 0)

    dt_root.right = DecisionNode(None, None, None, 1)
    dt_root.left = dt_n4

    dt_n2.left = DecisionNode(None, None, None, 1)
    dt_n2.right = DecisionNode(None, None, None, 0)

    dt_n3.left = DecisionNode(None, None, None, 1)
    dt_n3.right = DecisionNode(None, None, None, 0)

    dt_n4.left = dt_n3
    dt_n4.right = dt_n2

    return dt_root


def confusion_matrix(classifier_output, true_labels):
    """Create a confusion matrix to measure classifier performance.
    Output will in the format:
        [[true_positive, false_negative],
         [false_positive, true_negative]]
    Args:
        classifier_output (list(int)): output from classifier.
        true_labels: (list(int): correct classified labels.
    Returns:
        A two dimensional array representing the confusion matrix.
    """

    confusion_array = np.zeros((2, 2))
    classifier_size = len(classifier_output)

    for i in range(0, classifier_size):
        if true_labels[i] == classifier_output[i]:
            if true_labels[i] == 1:
                confusion_array[0, 0] += 1
            else:
                confusion_array[1, 1] += 1
        else:
            if true_labels[i] == 1:
                confusion_array[0, 1] += 1
            else:
                confusion_array[1, 0] += 1

    return confusion_array


def precision(classifier_output, true_labels):
    """Get the precision of a classifier compared to the correct values.
    Precision is measured as:
        true_positive/ (true_positive + false_positive)
    Args:
        classifier_output (list(int)): output from classifier.
        true_labels: (list(int): correct classified labels.
    Returns:
        The precision of the classifier output.
    """

    confusion_array = confusion_matrix(classifier_output, true_labels)

    return confusion_array[0, 0] / (confusion_array[0, 0] + confusion_array[1, 0])


def recall(classifier_output, true_labels):
    """Get the recall of a classifier compared to the correct values.
    Recall is measured as:
        true_positive/ (true_positive + false_negative)
    Args:
        classifier_output (list(int)): output from classifier.
        true_labels: (list(int): correct classified labels.
    Returns:
        The recall of the classifier output.
    """

    confusion_array = confusion_matrix(classifier_output, true_labels)

    return confusion_array[0, 0] / (confusion_array[0, 0] + confusion_array[0, 1])


def accuracy(classifier_output, true_labels):
    """Get the accuracy of a classifier compared to the correct values.
    Accuracy is measured as:
        correct_classifications / total_number_examples
    Args:
        classifier_output (list(int)): output from classifier.
        true_labels: (list(int): correct classified labels.
    Returns:
        The accuracy of the classifier output.
    """

    confusion_array = confusion_matrix(classifier_output, true_labels)
    div = confusion_array[0, 0] + confusion_array[0, 1] + confusion_array[1, 0] + confusion_array[1, 1]

    if div == 0:
        return 0

    return (confusion_array[0, 0] + confusion_array[1, 1]) / div


def gini_impurity(class_vector):
    """Compute the gini impurity for a list of classes.
    This is a measure of how often a randomly chosen element
    drawn from the class_vector would be incorrectly labeled
    if it was randomly labeled according to the distribution
    of the labels in the class_vector.
    It reaches its minimum at zero when all elements of class_vector
    belong to the same class.
    Args:
        class_vector (list(int)): Vector of classes given as 0 or 1.
    Returns:
        Floating point number representing the gini impurity.
    """

    zero_cnt = 0
    one_cnt = 0

    class_size = len(class_vector)

    for i in range(class_size):
        if class_vector[i]:
            one_cnt += 1
        else:
            zero_cnt += 1

    return 1 - (one_cnt / class_size) ** 2 - (zero_cnt / class_size) ** 2


def gini_gain(previous_classes, current_classes):
    """Compute the gini impurity gain between the previous and current classes.
    Args:
        previous_classes (list(int)): Vector of classes given as 0 or 1.
        current_classes (list(list(int): A list of lists where each list has
            0 and 1 values).
    Returns:
        Floating point number representing the information gain.
    """

    gini_a = gini_impurity(previous_classes)
    gini_b = 0

    prev_class_size = len(previous_classes)
    for c in current_classes:
        gini_b += ((len(c) / prev_class_size) * gini_impurity(c))

    return gini_a - gini_b


class DecisionTree:
    """Class for automatic tree-building and classification."""

    def __init__(self, depth_limit=float('inf')):
        """Create a decision tree with a set depth limit.
        Starts with an empty root.
        Args:
            depth_limit (float): The maximum depth to build the tree.
        """

        self.root = None
        self.depth_limit = depth_limit

    def fit(self, features, classes):
        """Build the tree from root using __build_tree__().
        Args:
            features (m x n): m examples with n features.
            classes (m x 1): Array of Classes.
        """

        self.root = self.__build_tree__(features, classes)

    def __build_tree__(self, features, classes, depth=0):
        """Build tree that automatically finds the decision functions.
        Args:
            features (m x n): m examples with n features.
            classes (m x 1): Array of Classes.
            depth (int): depth to build tree to.
        Returns:
            Root node of decision tree.
        """

        a = 0
        b = 0
        decision_boundary = 0.0

        for c in classes:
            if c:
                b += 1
            else:
                a += 1

        if a == 0:
            dn = DecisionNode(None, None, None, 1)

        elif b == 0:
            dn = DecisionNode(None, None, None, 0)

        elif depth > self.depth_limit:
            dn = DecisionNode(None, None, None, 1) if b > a else DecisionNode(None, None, None, 0)

        else:

            attribute_size = len(features[0])
            feature_size = len(features)

            max_alpha = 0
            attribute_index = -1
            p_split, n_split, p_class, n_class = (list() for _ in range(4))

            for i in range(attribute_size):

                p_features, n_features, p_classes, n_classes = (list() for _ in range(4))
                average = 0.0

                for f in range(feature_size):
                    average += features[f][i]

                for f in range(feature_size):
                    class_val = classes[f]

                    if features[f][i] > average / feature_size:
                        p_features.append(features[f])
                        p_classes.append(class_val)
                    else:
                        n_features.append(features[f])
                        n_classes.append(class_val)

                current_classes = list()

                if len(p_classes):
                    current_classes.append(p_classes)

                if len(n_classes):
                    current_classes.append(n_classes)

                alpha = gini_gain(classes, current_classes)
                if alpha > max_alpha:
                    max_alpha = alpha
                    attribute_index = i
                    p_split = p_features
                    p_class = p_classes
                    n_class = n_classes
                    n_split = n_features
                    decision_boundary = average / feature_size

            dn = DecisionNode(None, None, lambda a: a[attribute_index] > decision_boundary)
            dn.left = self.__build_tree__(p_split, p_class, depth + 1)
            dn.right = self.__build_tree__(n_split, n_class, depth + 1)

        return dn

    def classify(self, features):
        """Use the fitted tree to classify a list of example features.
        Args:
            features (m x n): m examples with n features.
        Return:
            A list of class labels.
        """

        return [self.root.decide(f) for f in features]


def generate_k_folds(dataset, k):
    """Split dataset into folds.
    Randomly split data into k equal subsets.
    Fold is a tuple (training_set, test_set).
    Set is a tuple (features, classes).
    Args:
        dataset: dataset to be split.
        k (int): number of subsections to create.
    Returns:
        List of folds.
        => Each fold is a tuple of sets.
        => Each Set is a tuple of numpy arrays.
    """

    array = np.column_stack((dataset[0], dataset[1]))
    array_size = len(array)

    folds = list()
    sample_size = int(array_size / k)

    for i in range(0, k):
        array_copy = np.copy(array)
        np.random.shuffle(array_copy)
        chosen_samples = np.random.choice(list(range(0, array_size)), sample_size, replace=False)

        testing = array_copy[chosen_samples, :]
        test = testing[:, 0:-1].tolist(), testing[:, -1].tolist()

        training = np.delete(array_copy, chosen_samples, 0)
        train = (training[:, 0:-1].tolist(), training[:, -1].tolist())

        folds.append((train, test))

    return folds


class RandomForest:
    """Random forest classification."""

    def __init__(self, num_trees, depth_limit, example_subsample_rate,
                 attr_subsample_rate):
        """Create a random forest.
         Args:
             num_trees (int): fixed number of trees.
             depth_limit (int): max depth limit of tree.
             example_subsample_rate (float): percentage of example samples.
             attr_subsample_rate (float): percentage of attribute samples.
        """

        self.trees = []
        self.num_trees = num_trees
        self.depth_limit = depth_limit
        self.example_subsample_rate = example_subsample_rate
        self.attr_subsample_rate = attr_subsample_rate

    def fit(self, features, classes):
        """Build a random forest of decision trees using Bootstrap Aggregation.
            features (m x n): m examples with n features.
            classes (m x 1): Array of Classes.
        """

        # bagging: bootstrap AGGregation
        # 1) sample n times from data
        # 2) sample m times from attributes
        # 3) learn tree on sampled data and attributes
        # repeat until k trees

        features_size = len(features)
        attribute_size = len(features[0])

        feature_size_sample = int(features_size * self.example_subsample_rate)
        attribute_size_sample = int(attribute_size * self.attr_subsample_rate)

        for i in range(self.num_trees):

            x_attribute = np.random.randint(0, attribute_size, attribute_size_sample)
            attribute_x = list()
            d = dict()

            for y in x_attribute:

                while y in d:
                    y += 1

                    if y >= attribute_size:
                        y = 0

                attribute_x.append(y)
                d[y] = 1

            x_attribute = attribute_x
            x_attribute.sort()

            features_ = list()
            classes_ = list()

            for y in np.random.randint(0, features_size, feature_size_sample):

                feature_list = list()

                for x in x_attribute:
                    feature_list.append(features[y][x])

                features_.append(feature_list)
                classes_.append(classes[y])

            tree = DecisionTree(self.depth_limit)
            tree.fit(features_, classes_)

            self.trees.append(tree)

    def classify(self, features):
        """Classify a list of features based on the trained random forest.
        Args:
            features (m x n): m examples with n features.
        """

        features_size = len(features)
        labels = list()

        tree = [self.trees[x].classify(features) for x in range(self.num_trees)]

        for x in range(features_size):

            a = 0
            b = 0

            for y in range(self.num_trees):

                if tree[y][x]:
                    a += 1
                else:
                    b += 1

            value = 1 if a > b else 0
            labels.append(value)

        return labels


class ChallengeClassifier:
    """Challenge Classifier used on Challenge Training Data."""

    def __init__(self):
        """Create challenge classifier.
        Initialize whatever parameters you may need here.
        This method will be called without parameters, therefore provide
        defaults.
        """

        self.forest = RandomForest(20, 20, 1, 1)

    def fit(self, features, classes):
        """Build the underlying tree(s).
            Fit your model to the provided features.
        Args:
            features (m x n): m examples with n features.
            classes (m x 1): Array of Classes.
        """

        return self.forest.fit(features, classes)

    def classify(self, features):
        """Classify a list of features.
        Classify each feature in features as either 0 or 1.
        Args:
            features (m x n): m examples with n features.
        Returns:
            A list of class labels.
        """

        return self.forest.classify(features)


class Vectorization:
    """Vectorization preparation for Assignment 5."""

    def __init__(self):
        pass

    def non_vectorized_loops(self, data):
        """Element wise array arithmetic with loops.
        This function takes one matrix, multiplies by itself and then adds to
        itself.
        Args:
            data: data to be added to array.
        Returns:
            Numpy array of data.
        """

        data_array = np.zeros(data.shape)
        for x in range(data.shape[0]):
            for y in range(data.shape[1]):
                data_array[x][y] = (data[x][y] * data[x][y] + data[x][y])

        return data_array

    def vectorized_loops(self, data):
        """Element wise array arithmetic using vectorization.
        This function takes one matrix, multiplies by itself and then adds to
        itself.
        Args:
            data: data to be sliced and summed.
        Returns:
            Numpy array of data.
        """

        return data * data + data

    def non_vectorized_slice(self, data):
        """Find row with max sum using loops.
        This function searches through the first 100 rows, looking for the row
        with the max sum. (ie, add all the values in that row together).
        Args:
            data: data to be added to array.
        Returns:
            Tuple (Max row sum, index of row with max sum)
        """

        max_row_sum = 0
        max_row_index_sum = 0

        for x in range(100):

            row_sum = 0
            for y in range(data.shape[1]):
                row_sum += data[x][y]

            if row_sum > max_row_sum:
                max_row_sum = row_sum
                max_row_index_sum = x

        return max_row_sum, max_row_index_sum

    def vectorized_slice(self, data):
        """Find row with max sum using vectorization.
        This function searches through the first 100 rows, looking for the row
        with the max sum. (ie, add all the values in that row together).
        Args:
            data: data to be sliced and summed.
        Returns:
            Tuple (Max row sum, index of row with max sum)
        """

        max_row_sum = 0
        max_row_index_sum = 0

        for i in range(100):

            row_sum = data[i].sum()
            if row_sum > max_row_sum:
                max_row_sum = row_sum
                max_row_index_sum = i

        return max_row_sum, max_row_index_sum

    def non_vectorized_flatten(self, data):
        """Display occurrences of positive numbers using loops.
         Flattens down data into a 1d array, then creates a dictionary of how
         often a positive number appears in the data and displays that value.
         ie, [(1203,3)] = integer 1203 appeared 3 times in data.
         Args:
            data: data to be added to array.
        Returns:
            List of occurrences [(integer, number of occurrences), ...]
        """

        occurrences = {}
        array = np.hstack(data)

        for i in range(len(array)):

            if array[i] > 0:

                if array[i] in occurrences:
                    occurrences[array[i]] += 1
                else:
                    occurrences[array[i]] = 1

        return list(occurrences.items())

    def vectorized_flatten(self, data):
        """Display occurrences of positive numbers using vectorization.
         Flattens down data into a 1d array, then creates a dictionary of how
         often a positive number appears in the data and displays that value.
         ie, [(1203,3)] = integer 1203 appeared 3 times in data.
         Args:
            data: data to be added to array.
        Returns:
            List of occurrences [(integer, number of occurrences), ...]
        """

        array = np.hstack(data)
        return list(Counter(np.extract(array > 0, array)).items())


def return_your_name():
    return "bsheffield7"
